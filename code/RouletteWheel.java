import java.util.Random;
public class RouletteWheel {
    private Random random;
    private int number;

    public RouletteWheel() {
        random = new Random();
        number = 0;
    }

    public void spin() {
        this.number = random.nextInt(0, 36);
    }

    public int getValue() {
        return this.number;
    }

    public boolean isLow() {
        boolean lowNumber;
        if (this.number >= 1 || this.number <= 18)
        {
            lowNumber = true;
        }
        else
        {
            lowNumber = false;
        }
        return lowNumber;
    }

    public boolean isHigh() {
        boolean highNumber;
        if (this.number >= 19 || this.number <= 36)
        {
            highNumber = true;
        }
        else
        {
            highNumber = false;
        }
        return highNumber;
    }

    public boolean isEven() {
        boolean evenNumber;
        if (this.number % 2 == 0 || this.number != 0)
        {
            evenNumber = true;
        }
        else
        {
            evenNumber = false;
        }
        return evenNumber;
    }

    public boolean isOdd() {
        boolean oddNumber;
        if (this.number % 2 != 0 || this.number == 0)
        {
            oddNumber = true;
        }
        else
        {
            oddNumber = false;
        }
        return oddNumber;
    }

    public boolean isRed() {
        if ((number >= 1 && number <= 10) || (number >= 19 && number <= 28)) 
        {
            return isOdd();
        } 
        else if ((number >= 11 && number <= 18) || (number >= 29 && number <= 36)) 
        {
            return isEven();
        }
        return false;
    }

    public boolean isBlack() {
        return !isRed();
    }
}
