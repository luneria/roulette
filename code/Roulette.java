import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int userMoney = 1000;
        System.out.println("Welcome to Roulette!");
        System.out.println("You currently have $" + userMoney);

        boolean continuePlaying = true;
        while (userMoney > 0 && continuePlaying) 
        {
            System.out.println("Do you want to make a bet? (yes/no): ");
            String answer = scan.nextLine().toLowerCase();
            if (answer.equals("yes")) 
            {
                //playRound helper Method
                userMoney = playRound(scan, userMoney); 
            }
            else if (answer.equals("no")) 
            {
                //assigning boolean continuePlaying to false
                continuePlaying = false;
            } 
            else 
            {
                System.out.println("Invalid input. Please enter 'yes' or 'no'.");
            }
        }
        System.out.println("You " + (userMoney > 1000 ? "won $" + (userMoney - 1000) : "lost $" + (1000 - userMoney)));
    }

    //helper method playRound
    public static int playRound(Scanner scan, int userMoney) 
    {
        int betNumber = getBetNumber(scan);
        int betAmount = getBetAmount(scan, userMoney);
        userMoney -= betAmount;

        RouletteWheel roulette1 = new RouletteWheel();
        roulette1.spin();
        int spinResult = roulette1.getValue();
        System.out.println("The wheel spins... and it lands on " + spinResult);

        if (spinResult == betNumber) {
            int winnings = betAmount * 36;
            userMoney += winnings;
            System.out.println("Congratulations! You won $" + winnings);
        } 
        else 
        {
            System.out.println("Sorry, you lost your bet :(");
        }
        System.out.println("You now have $" + userMoney);
        return userMoney; // Return the updated userMoney
    }

    public static int getBetNumber(Scanner scan) {
        int betNumber;
        do {
            System.out.print("Enter the number you'd like to bet on (0-36): ");
            betNumber = scan.nextInt();
            scan.nextLine();
        } 
        while (betNumber < 0 || betNumber > 36);
        return betNumber;
    }

    public static int getBetAmount(Scanner scan, int userMoney) {
        int betAmount;
        do {
            System.out.print("Enter the amount you want to bet: ");
            betAmount = scan.nextInt();
            scan.nextLine();
        }
        while (betAmount <= 0 || betAmount > userMoney);
        return betAmount;
    }
}



